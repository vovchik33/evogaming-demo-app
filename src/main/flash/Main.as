package {
    import flash.display.MovieClip;
    import flash.events.Event;

    public class Main extends MovieClip {
        public function Main():void
        {
            trace("Hello, world!!!!");
            this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        }

        private function addedToStageHandler(e:Event):void
        {
            this.graphics.beginFill(0xFF0000);
            this.graphics.drawRect(0,0,100,200);
            this.graphics.endFill();
        }
    }
}