package {
	import flash.display.MovieClip;

	import com.demonsters.debugger.MonsterDebugger;

	import org.as3commons.logging.api.LOGGER_FACTORY;
	import org.as3commons.logging.api.getLogger;

	import org.as3commons.logging.setup.LevelTargetSetup;
	import org.as3commons.logging.setup.LogSetupLevel;
	import org.as3commons.logging.setup.mergeSetups;
	import org.as3commons.logging.setup.target.TraceTarget;
	import org.as3commons.logging.setup.target.MonsterDebugger3TraceTarget;
	import org.as3commons.logging.setup.target.mergeTargets;

	use namespace LOGGER_FACTORY;

	public class PictureGalleryApp extends MovieClip {
		public function PictureGalleryApp():void {
			MonsterDebugger.initialize(this);
			try {
                LOGGER_FACTORY.setup = mergeSetups(
					new LevelTargetSetup(new MonsterDebugger3TraceTarget(), LogSetupLevel.INFO)
                );
            } catch (err:Error) {
                trace("Unable to setup logging system! Please, restart application once again!", "Error");
            }
			MonsterDebugger.enabled = true;
			getLogger(this).info("Hello, WOrld");
		}
	}
}