EXTRA TASK #1

*Note*: In order to run the unit tests, Flexmojos needs to be able to find the Flashplayer executable. You should ensure Flashplayer (`Flash Player` on OS X and `Flashplayer.exe` on WinX) is available on your PATH. You can download the standalone Flashplayer *Projector* from [Adobe](http://www.adobe.com/support/flashplayer/downloads.html). In Windows, you can copy Flashplayer.exe to some location and add it to your PATH via `My Computer > Advanced > Environment Variables`. On OS X, I installed `Flash Player` to `usr/local/bin` and added the following to my `~/.bash_profile`

	export FLASH_PLAYER='/usr/local/bin/Flash Player.app/Contents/MacOS'
	export PATH=${M2_HOME}/bin:$FLASH_PLAYER:${PATH}

For more information, check out [this post](https://docs.sonatype.org/display/FLEXMOJOS/Running+unit+tests).

EXTRA TASK #2

private function concatSortedArrays(a1:Array=null, a2:Array=null):Array {
	var index1:int = 0;	//current index of a1 array
	var index2:int = 0; //current index of a2 array
	var length1:int = (a1 != null)?a1.length:0; //length of a1 array
	var length2:int = (a2 != null)?a2.length:0; //length of a2 array
	
	var lengthResultArray:int = length1 + length2; //length of result array
	
	var result:Array = new Array(lengthResultArray); //result array
	
	while ((index1 + index2) < lengthResultArray) { //while we have non-processed elements at least in one array 
		if (index1==length1) { //first array already processed
			result[index1 + index2] = a2[index2];
			index2++;
		} else if (index2==length2) { //second array already processed
			result[index1 + index2] = a1[index1];
			index1++;
		} else { //both arrays have non-processed elements
			if (a1[index1] == a2[index2]) { //both arrays have equal elements on current positions - we can add both of them at the same time
				result[index1 + index2] = a1[index1];
				index1++;
				result[index1 + index2] = a2[index2];
				index2++;
			} else if (a1[index1] < a2[index2]) {//add element from array a1 on current position index1 onto current position of result array
				result[index1 + index2] = a1[index1];
				index1++;
			} else {//add element from array a2 on current position index2 onto current position of result array
				result[index1 + index2] = a2[index2];
				index2++;
			}
		}
	}
	return result;
}

EXTRA TASK #3

var xmlData:XML =
                <root>
                                <node myVal="1">data</node>
                                <node myVal="2"> data </node>
                                <node myVal="3"> data </node>
                                <node myVal="4"> data </node>
                                <node myVal="5"> data </node>
                </root>;

xmlData.children().(@myVal % 2 && trace(@myVal));

It creates xml object and keep it in variable xmlData. It applies filter (operator ()) to child nodes received with method elemenets() in second string. During filter applying it traces value of @myVal attribute for each node, which myVal attribute value could be converted to integer value and remainder of division by 2 is equal to 1

Result:
1
3
5

On each iteration it checks condition (@myVal % 2 && trace(@myVal))
First of all implicit type conversion occurs on expression @myVal. It tries to convert it to number value before dividing by 2, and calculate the result of expression (@myVal % 2). After that It tries to convert this expression to Boolean and calculate the result of expression (@myVal % 2 && trace(@myVal)). When (@myVal % 2) after implicit type conversion is equal to false it stops to calculate second part of expression (trace(@myVal)) and @myVal won't be printed in that case. When (@myVal % 2) after implicit type conversion is equal to true< it will try to calculate second part of expression (trace(@myVal)), it will print @myVal and return undefined. So, anyway (@myVal % 2 && trace(@myVal)) is equal to false and xmlData.children().(@myVal % 2 && trace(@myVal)) will give us empty xml.

This code generates error when we have any nodes without attribute myVal.
Result of execution depends on type conversion. We have to be sure that all attributes myVal in child nodes have an integer values.

===========================================================================================================

Official page:

  https://flexmojos.atlassian.net/wiki/spaces/FLEXMOJOS/overview

Set path to Maven:

    execute vim ~/.bash_profile
    add path (for example export PATH=$PATH:/Users/admin/Downloads/apache-maven-3.5.0/bin)
    save and exit 

Set path to FlashPlayer for macOS in maven settings(/Users/admin/Downloads/apache-maven-3.5.0/conf/settings.xml and /Users/admin/.m2/settings.xml):
    
    <profile>
      <id>test</id>
      <properties>
        <flex.flashPlayer.command>/Applications/Adobe Animate CC 2018/Players/Flash Player.app/Contents/MacOS/Flash Player</flex.flashPlayer.command>
      </properties>
    <profile>
